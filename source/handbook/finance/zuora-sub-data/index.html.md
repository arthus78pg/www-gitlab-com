---
layout: markdown_page
title: "Zuora Subscription Data Management"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### New Accounts vs New Subscriptions

There are instances where a new account in Zuora is required rather than just a new subscription in an existing account. This is determined by the sold-to contact person. 

Within the customer account portal, customers can only see a single Zuora account at a time. If a customer wants to add a subscription and the contact information is the same, then the subscription can be added to the existing account. 

If a customer wants an additional subscription for a different sold to contact, then a new Zuora account will be created so that every sold to contact can log into the portal and manage their subscriptions.

### Linking Renewal Subscriptions

When a customer renews their subscription, a new subscription is typically created. This can create challenges for calculating metrics like dollar retention for a subscription because once subscription has ended and another has started. To address this, a linkage is made between the original subscription and its renewal(s). 

The field `Renewal subscription` is used to create the mapping. These are the following constraints on this field:
* This field defines a unidirectional relationship that points to a separate subscription name. 
* A renewal subscription can start on the same or future day as the original subscription start date to which it is linked to, but never in the past. 
* Any number of subscriptions can point to the same renewal subscription as long as the time constraint is met. 
* A subscription may have any number of renewal subscriptions that it points to as long as the time constraint is met. This is a one-to-many relationship. Each renewal subscription to which the original subscription is linked is input in the field and are separated by ` || `. 
  * For example, subscription [A-S00009093](https://www.zuora.com/apps/Subscription.do?method=view&id=2c92a0ff635c92e601635fdb126b3967) is linked to `A-S00009096 || A-S00009095`
* Renewal subscriptions can point to subscriptions under separate Zuora Accounts
* Renewal subscriptions can start 12 months or less after the original subscription. Practically, this is because a linkage of greater than 12 months has no effect on any relevant metrics (Retention or Yearly counts).

The process to make the linkage is as follows:
1. Cancel the old subscription in Zuora.
1. Copy and paste the new subscription in "Renewal Subscription" field with no trailing spaces.
