---
layout: markdown_page
title: "Geo and Disaster Recovery"
description: "Summary of how the Geo Team operates"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## The Geo Team

[Geo](/features/gitlab-geo/) is a [Premium](/pricing/) feature, built to help speed up the development of distributed teams by providing
one or more read-only mirrors of a primary GitLab instance. This mirror (a Geo secondary node) reduces the time to clone or fetch large
repositories and projects, or can be part of a Disaster Recovery solution.

### Team members

<%= direct_team(manager_role: 'Engineering Manager, Geo') %>

### Stable counterparts

<%= stable_counterparts(role_regexp: /[,&] Geo/, direct_manager_role: 'Engineering Manager, Geo') %>

## Goals and Priorities

Our priorities are aligned with the product direction. You can read more about this on the [Geo Product Vision page](https://about.gitlab.com/direction/geo/).

Alongside the items listed in our Product Vision, we need to constantly assess issues that our customers bring to our
attention. These could take the form of bug reports or feature requests. Geo users are often our largest
customers and some rely on Geo as a critical part of their workflow.

We also work constantly to keep housekeeping chores to a manageable level. Where possible, we address these issues
as part of a related project. Where this is not possible, we use time around our projects to make this happen.

## Geo's Relationship to Disaster Recovery

Disaster Recovery (DR) is a set of policies, tools and procedures put in place to be able to recover from a disaster. 

Geo provides data redundancy. The customer will have a redundant copy of data in a separate location. If anything were to happen to their primary instance, a secondary instance still retains a copy of the data. 

However, data redundancy is one part of a complete DR strategy. 

High Availability (HA) is also a step towards Disaster Recovery. At the moment Geo does not provide true HA because if the primary instance is not available, certain actions are not possible.

## Common Links

Documentation
- [Geo](https://docs.gitlab.com/ee/gitlab-geo/)
- [Disaster Recovery](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/index.html)
- [Planned Failover](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/planned_failover.html)
- [Background Verification](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/background_verification.html)

Other Resources
- Issues relating to Geo are mostly to be found on the
[gitlab-ee issue tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues/?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Geo)
- [Chat channel](https://gitlab.slack.com/archives/g_geo); please use the `#g_geo`
chat channel for questions that don't seem appropriate to use the issue tracker
for.

## Geo Terminology

| Term  |  Definition |  
|---|---|
| Geo |  The product name given to the feature that provides the ability to create one or more read-only mirrors for the main/primary instance |
| Primary  | The main, primary instance where read-write operations are allowed |
| Secondary  | An instance that synchronizes with the Primary node where only read-only operations are permitted |

## Planning and Demos

### Discussions

Discussions are documented [separately](https://docs.google.com/document/d/18vGk6dQs7L0oGQOb_bNiFa5JhwLq5WBS7oNxQy09ml8/edit#heading=h.p295wb40mdh4).

### Planning

We use issue boards to focus on each milestone and a [planning board](https://gitlab.com/groups/gitlab-org/-/boards/796972?&label_name[]=Geo) to look further ahead.  

[Here](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Geo) you can find the Epics for Geo.

We start planning the new milestone around the 15th of each month. During this week we open up the new board and start adding issues. Individual team members
are notified and asked to add weights to each issue. Weight is used for capacity planning and to highlight issues of high complexity.

We use the weights from the set [1, 3, 5, 10, 20] where the value represents a "best day". For example, if an issue is assigned a weight of 5 then an engineer 
should be able to complete the work in 5 days if this were the only thing that they needed to do for those five days. If an issue is assigned a weight of 20, 
then the issue is too big and will need to be broken down further. Issues weighted at 10 will also need a rough to-do list added to the issue. (An issue of this size 
probably doesn't need multiple related issues, but a to-do list will help understand how the 10 points need to be spent.) You can assign a weight of 20 if there 
is a lot of uncertainty around the issue and more planning is needed. 

When the new milestone is almost finalized, a planning issue is created in the [geo-team project](https://gitlab.com/groups/geo-team/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Milestone%20Planning). 
[This is an example of a plannning issue](https://gitlab.com/geo-team/planning-discussions/issues/6). The team is tagged in the issue and all team members will read through the issues to get an overview
of what is planned, along with the estimated weight. If anyone has a concern about the weight of an issue, they are expected to start a discussion on the issue
to confirm that there are no misunderstandings about the requirements. This was the simplest approach we could find to collaborate on assigning weight to issues. 

### Demos

The demos are recorded and should be stored in Google Drive under "GitLab Videos --> [Geo Demos](https://drive.google.com/drive/u/0/folders/1Ot2ElWwEh9vdPx1K8VO5ZMBkxlmRAXm4)". If you recorded the demo, please make sure the recording ends up in that folder.
