---
layout: markdown_page
title: "Blueprints"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

**Blueprints** are intended to flesh out our thinking about specific ideas we need to implement and/or problems we need to solve. 

* Blueprints are required for, but not limited to, KRs. All KRs require a blueprint, as they define, scope, and outline additional considerations, options and recommendations for a KR. They should result in a clear understanding of the topic and widespread agreement on the specific deliverables that implement a KR. 

* Blueprints can also be associated with items other than KRs: whenever we are considering whether we should tackle a complex problem and how we might approach it, a blueprint is recommended. 

* Blueprints are sketches whose purpose is to foster and frame discussion around Infrastructure topics. They  are one of the inputs to [DNA meetings](../meeting/#design-and-automation-dna), and will generally yield one of more [designs](/handbook/engineering/infrastructure/design/).

As a guideline, a blueprint are structured as follows:

* Idea or problem statement: provide background and scope the idea or problem discussed in the blueprint
* Summary of past efforts (if applicatble)
* Outline of options
* Additional considerations
  * Dependencies
  * Related blueprints or designs
  * Costs
* Recommendations

## Index

Listed alphabetically:

* [Deltas](deltas/)
* [Dogfooding CI/CD](ci-cd/)
* [OKRs](okrs/)
* [Planning Workflow](planning/)
* [Security Releases](release/security/)
* [Service Levels and Error Budgets](service-levels-error-budgets/)
* [Storage](storage/)
