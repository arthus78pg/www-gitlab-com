/* global setupCountdown */

function setupHackathonCountdown() {
  var nextHackathonDate = new Date('February 12, 2019 08:00:00').getTime();

  setupCountdown(nextHackathonDate, 'nextHackathonCountdown');
}

(function() {
  setupHackathonCountdown();
})();

